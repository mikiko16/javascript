import config from '../config';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
  constructor() {
    super();

    this.config = config;
    this.data = {};

    this.init();
  }

  static get events() {
    return EVENTS;
  }

  /**
   * Initializes the app.
   * Called when the DOM has loaded. You can initiate your custom classes here
   * and manipulate the DOM tree. Task data should be assigned to Application.data.
   * The APP_READY event should be emitted at the end of this method.
   */
  async init() {
    // Initiate classes and wait for async operations here.

    var opts = {
      method: 'GET',      
      headers: {}
    };
  
  await fetch('https://swapi.booost.bg/api/planets/', opts).
        then(function (response) {
        return response.json();
    })
    .then((body) => {
      let counts = body.count;

      this.data.count = counts; 
    });

    let planets = [];

    for(let i = 1; i <= 60; i++) {
      await fetch('https://swapi.booost.bg/api/planets/' + i.toString(), opts)
      .then(function (response) {
          return response.json();
          })
      .then(async (body) => {

        planets.push(body);

        this.data.planets = planets;        
      })  
      .catch((err) =>
      {
        console.log(err);
      });
    }

  this.emit(Application.events.APP_READY); 
  }
}

